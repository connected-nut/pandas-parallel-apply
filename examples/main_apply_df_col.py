import pandas as pd
from pandas_parallel_apply import DataFrameParallel, SeriesParallel
from datetime import datetime
import numpy as np

data = np.random.randn(1000000, 3)

df = pd.DataFrame(data, columns=["A", "B", 1])
print(df)

def f(x):
    return x + 99

print("___")
now = datetime.now()
asdf = df["A"].apply(f)
print(f"Took {datetime.now() - now}")

print("___")
now = datetime.now()
dfp = DataFrameParallel(df, pbar=False)
asdf2 = dfp["A"].apply(f)
print(f"Took {datetime.now() - now}")
assert np.allclose(asdf, asdf2)

print("___")
now = datetime.now()
asdf4 = SeriesParallel(df["A"], pbar=False).apply(f)
print(f"Took {datetime.now() - now}")
assert np.allclose(asdf, asdf4)
