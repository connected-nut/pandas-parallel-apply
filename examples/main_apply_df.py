from bdb import Breakpoint
import pandas as pd
from pandas_parallel_apply import DataFrameParallel
from datetime import datetime
import numpy as np

data = {
    "A": [1,2,3],
    "B": ["hello", "darkness", "hi"],
    1: [(1,2), (3,4), "yolo"]
}

data = np.random.randn(100000, 3)

df = pd.DataFrame(data, columns=["A", "B", 1])
print(df)

def f(x):
    return [x["A"] + 99, x["B"], x[1]]

print("___")
now = datetime.now()
asdf = df.apply(f, axis=1)
print(f"Took {datetime.now() - now}")

print("___")
now = datetime.now()
dfp = DataFrameParallel(df, pbar=False)
asdf2 = dfp.apply(f, axis=1)
print(f"Took {datetime.now() - now}")
assert np.allclose(np.concatenate(asdf), np.concatenate(asdf2))
