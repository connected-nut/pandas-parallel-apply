import pandas as pd
from pandas_parallel_apply import GroupByParallel
from datetime import datetime
import numpy as np

from pandas_parallel_apply.data_frame_parallel import DataFrameParallel


class Timer:
    def __init__(self, message: str = ""):
        self.message = message

    def __enter__(self):
        self.start = datetime.now()
    
    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.end = datetime.now()
        print(f"{self.message} took {self.end - self.start}")

data_static = {
    "A": [1,1,2,3,5,3],
    "B": ["hello", "darkness", "hi", "dafuq", "who", "asdf"],
    1: [(1,2), (3,4), "yolo", 1, 2, 3]
}

N = 10000000
data_random = {"A": np.random.randint(0, 10, size=(N, )), "B": np.arange(N),
    "C": [chr(ord("A") + np.random.randint(0, 26)) for _ in range(N)] }

def run_one_col_return_series(df):
    with Timer("Serial"):
        Y = df.groupby("A").apply(len)
    with Timer("Parallel OOP"):
        K = GroupByParallel(df.groupby("A"), pbar=False).apply(len)
    with Timer("Parallel OOP v2"):
        P = DataFrameParallel(df, pbar=False).groupby("A").apply(len)
    assert np.allclose(Y.values, K.values)
    assert np.allclose(Y.values, P.values)

def run_one_col_return_df(df):
    f = lambda df: df.iloc[0]
    with Timer("Serial"):
        Y = df.groupby("A").apply(f)
    with Timer("Parallel OOP"):
        K = GroupByParallel(df.groupby("A"), pbar=False).apply(f)
    with Timer("Parallel OOP v2"):
        P = DataFrameParallel(df, pbar=False).groupby("A").apply(f)
    assert (Y != K).sum().sum() == 0
    assert (Y != P).sum().sum() == 0

def run_one_col_return_tricky_df_no_index(df):
    def f(df):
        np.random.seed(42)
        N = np.random.randint(1, len(df) + 1)
        return df.iloc[0 : N]

    with Timer("Serial"):
        Y = df.groupby("A").apply(f)
    with Timer("Parallel OOP"):
        K = GroupByParallel(df.groupby("A"), pbar=False, keep_original_indexes=False).apply(f)
    with Timer("Parallel OOP v2"):
        P = DataFrameParallel(df, pbar=False).groupby("A").apply(f)
    assert (Y.values != K.values).sum() == 0
    assert (Y.values != P.values).sum() == 0

def run_one_col_return_tricky_df_plus_index(df):
    def f(df):
        np.random.seed(42)
        N = np.random.randint(1, len(df) + 1)
        return df.iloc[0 : N]

    with Timer("Serial"):
        Y = df.groupby("A").apply(f)
    with Timer("Parallel OOP"):
        K = GroupByParallel(df.groupby("A"), pbar=False, keep_original_indexes=True).apply(f)
    assert (Y != K).sum().sum() == 0

# # run_one_col_return_series(pd.DataFrame(data_static))
# run_one_col_return_series(pd.DataFrame(data_random))
# print("__________")

# # run_one_col_return_df(pd.DataFrame(data_static))
# run_one_col_return_df(pd.DataFrame(data_random))
# print("__________")

# # run_one_col_return_tricky_df_no_index(pd.DataFrame(data_static))
# run_one_col_return_tricky_df_no_index(pd.DataFrame(data_random))
# print("__________")

# run_one_col_return_tricky_df_plus_index(pd.DataFrame(data_static))
run_one_col_return_tricky_df_plus_index(pd.DataFrame(data_random))
print("__________")
